//create Table using dynamo Db 

resource "aws_dynamodb_table" "cars" {
  name = "cars" //Mandetory
  hash_key = "id" //Primary Key - Mandetory
  billing_mode = "PAY_PER_REQUEST"
  attribute {
    name = "id"
    type = "S" 
  } 
  //One of: S, N, or B for (S)tring, (N)umber or (B)inary data
  
}

//Create item in table 
resource "aws_dynamodb_table_item" "car-items" {
  table_name = aws_dynamodb_table.cars.name
  hash_key = aws_dynamodb_table.cars.hash_key
  item = <<EOF
  {
    "id" : {"S" : "CAR_001"},
    "model" : {"S" : "TOYOTA"},
    "yearOfMake" : {"N" : "2008"}
  }
  EOF
}