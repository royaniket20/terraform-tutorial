//Creating EC2 Machine

resource "aws_instance" "myEc2" {
  ami = "ami-03a6eaae9938c858c"
  instance_type = "t2.micro"
  tags = {
    name = "my automated machine"
    desc = "something is cooking"
    Name = "Welcome terraform" //Special Tag for showing in aws management console 
  }
  user_data = <<EOF
  #!/bin/bash
  echo "Hello World from aniket "
  EOF
# key_name = aws_key_pair.my-public-key.id
vpc_security_group_ids = [aws_security_group.my-sec-group.id]
}
//Aws keypair assignment
# resource "aws_key_pair" "my-public-key" {
#   public_key = file("public key need to give here ")
# }
//aws security group 
resource "aws_security_group" "my-sec-group" {
  name = "my-sec-group"
  description  = "Some of my own access"
  ingress {
    description      = "SSH access"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
//Printing Public Ip 
output "ny-ec2-public-Ip" {
  value = aws_instance.myEc2.public_ip
}