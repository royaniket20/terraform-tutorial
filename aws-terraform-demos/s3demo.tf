//crerating Bucket in S3 

resource "aws_s3_bucket" "finance-terraform-02021992" {
  bucket = "terraform-finance-terraform-02021992"
  tags = {
    messageTag = "terraform Dummy s3 Bucket"
  }
}
//Creating object inside bucket
resource "aws_s3_bucket_object" "finance-object" {
  source   = "dummyData.txt"
  key = "finance-object-key.txt"
  bucket = aws_s3_bucket.finance-terraform-02021992.id

}

//read informartion  from Aws exisiting group  
data "aws_iam_group" "developer-data" {
  group_name = "Developers"
}
//Printing extracted Data from developer group created manually 
output "developer-data-output" {
  value       = data.aws_iam_group.developer-data
  description = "Fetched IAM group"
}


//Creating s3 Bucket Policy ti Give access  to First user of Developer group 
resource "aws_s3_bucket_policy" "s3BucketPolicyForData" {
  bucket   = aws_s3_bucket.finance-terraform-02021992.id
  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Principal": {
        "AWS" : [
          "${data.aws_iam_group.developer-data.users[0].arn}" 
        ]
      },
			"Effect": "Allow",
			"Action": "*",
			"Resource": "arn:aws:s3:::${aws_s3_bucket.finance-terraform-02021992.id}/*"
		}
	]
}
  EOF
}



//NOW PUT SOME PROVIDER 
//This is also a meta argument

//Hardcoding credentials is bad - We need to read it from credentials store
//Either configure credentails in C:\Users\royan\.aws Folder with config and credentials
/**
config
[default]
region = us-east-1
output = json
credentials 
[default]
aws_access_key_id = AKIAWG6NY62EWUBPDVNC
aws_secret_access_key = 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

**/
/**
OR you can use environment variables - In that case Just entire provider can be ignored 
export AWS_ACCESS_KEY_ID = **
export AWS_SECRET_ACCESS_KEY_ID = **
export AWS_REGION = **
**/
# provider "aws" {
#   region = "us-east-1"
#   //access_key = "AKIAWG6NY62EWUBPDVNC"
#   //secret_key = "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"
# }
