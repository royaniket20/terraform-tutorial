//crerating user 

resource "aws_iam_user" "admin-user" {
  name = "terraform_dummy_user"
  tags = {
    messageTag = "terraform Dummy user "
  }
}
//Creating Ploict 
resource "aws_iam_policy" "S3User" {
  name   = "S3UserPloicy"
  policy = <<EOF
{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["s3:Get*","s3:List*","s3:Describe*","s3-object-lambda:Get*","s3-object-lambda:List*"],"Resource":"*"}]}
EOF
  //policy = file("some_policy.json")

}
//Attach Policy to IMA user created 
resource "aws_iam_user_policy_attachment" "applyPolicyToUser" {
  user       = aws_iam_user.admin-user.name //Extract Name of User when User created
  policy_arn = aws_iam_policy.S3User.arn    //Get Policy arn when thats created 
  //This will attache Policy to user
}



//NOW PUT SOME PROVIDER 
//This is also a meta argument

//Hardcoding credentials is bad - We need to read it from credentials store
//Either configure credentails in C:\Users\royan\.aws Folder with config and credentials
/**
config
[default]
region = us-east-1
output = json
credentials 
[default]
aws_access_key_id = AKIAWG6NY62EWUBPDVNC
aws_secret_access_key = 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

**/
/**
OR you can use environment variables - In that case Just entire provider can be ignored 
export AWS_ACCESS_KEY_ID = **
export AWS_SECRET_ACCESS_KEY_ID = **
export AWS_REGION = **
**/
provider "aws" {
  region = "us-east-1"
  //access_key = "AKIAWG6NY62EWUBPDVNC"
  //secret_key = "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"
}
