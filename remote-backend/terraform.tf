//We can use constrained version 

terraform {
  //Here backend is used as remote state management 
  backend "s3" {
    bucket = "terraform-tfstate-bucket-aniket"
    key = "terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "tfstate-locaking-management"
  }
  required_providers {
    local  = {
      source = "hashicorp/local"
      //version = "1.4.0"
      //version = "!=1.4.0" //Use the previous Version to this 
       //version = " >1.4.0"
        //version = "<1.4.0"
        //version = "~>1.2" //Either 1.2 upto 1.9  
        //version = "~>1.2.0" //Either 1.2.0 upto 1.2.9 
      version = ">1.2.0 , <2.0.0 ,!=1.4.0" //Combination  - So it will take 1.3.0
    }
  }
}
//May need to use terraform init -upgrade
