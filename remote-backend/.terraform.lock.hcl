# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "1.3.0"
  constraints = "> 1.2.0, != 1.4.0, < 2.0.0"
  hashes = [
    "h1:fVZRSMesg0ML5Dzz7639dzgihi3akow+/RX7kq25OQU=",
    "zh:19acbc592e081f56f9c1e10b9cb14ea35ed27f6260ea8f65def8e736bfd05383",
    "zh:6a7b71853c8a881990f6d2be09ea005d4d905e91d39dad0964faf03d97209bdb",
    "zh:7611944004e94464cc2119192a9683199d75fc161c5ad91a3e7e9c1d7e343d33",
    "zh:905b57dd793f1f045aad33a2ee5e47863d8e74e670de1f4f7358329f1b8e20af",
    "zh:94e0e2d0372ebbb83829db6888088b3e57500317c28dd3f0c057c9d2a7cf780e",
    "zh:9905db26ef0b77a83131b8ec75b608ab3d296335da4262858b9d4239cebd7576",
    "zh:a1c7c65ab3565e0a88a62b56427476a7866222f9f8b68f9cd616919bc36ddf9c",
    "zh:ad685238fd65423a30ccee23f105fda2fc02d5dd262662fad635b2d50266455d",
    "zh:bad3e49f50bbd5c6fb07248fdf26e05924e8f4be6c039d6a7e0fe9d5a2e9822b",
    "zh:d4652bbbfdf95bfb42549232031b3576dcab21ffe19e42486a1d62b7821298ae",
    "zh:e2a083af570fb740032f510f297d68ddbe71c3ea26733356061f5956eedc5b63",
    "zh:ea29e8ad1826394e0546a85f59f495094ac2e864248d49e980161f87efd0d008",
  ]
}
