//We can use constrained version 

terraform {
  required_providers {
    local  = {
      source = "hashicorp/local"
      //version = "1.4.0"
      //version = "!=1.4.0" //Use the previous Version to this 
       //version = " >1.4.0"
        //version = "<1.4.0"
        //version = "~>1.2" //Either 1.2 upto 1.9  
        //version = "~>1.2.0" //Either 1.2.0 upto 1.2.9 
      version = ">1.2.0 , <2.0.0 ,!=1.4.0" //Combination  - So it will take 1.3.0
    }
  }
}
//May need to use terraform init -upgrade


resource "local_file" "petsversioned" {
  filename        = "C:\\Users\\royan\\OneDrive\\Desktop\\Terroaform Tutorial\\local_terraform_experiment\\pets.txt"
  content         = "We love Aniket Roy"
  file_permission = "0700"
}

