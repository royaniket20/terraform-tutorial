resource "local_file" "petsVar" {
  filename        = var.filename
  content         = "We love Aniket Roy"
  file_permission = "0700"
}