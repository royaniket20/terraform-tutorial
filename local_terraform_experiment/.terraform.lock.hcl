# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "1.3.0"
  constraints = "> 1.2.0, != 1.4.0, < 2.0.0"
  hashes = [
    "h1:fVZRSMesg0ML5Dzz7639dzgihi3akow+/RX7kq25OQU=",
    "zh:19acbc592e081f56f9c1e10b9cb14ea35ed27f6260ea8f65def8e736bfd05383",
    "zh:6a7b71853c8a881990f6d2be09ea005d4d905e91d39dad0964faf03d97209bdb",
    "zh:7611944004e94464cc2119192a9683199d75fc161c5ad91a3e7e9c1d7e343d33",
    "zh:905b57dd793f1f045aad33a2ee5e47863d8e74e670de1f4f7358329f1b8e20af",
    "zh:94e0e2d0372ebbb83829db6888088b3e57500317c28dd3f0c057c9d2a7cf780e",
    "zh:9905db26ef0b77a83131b8ec75b608ab3d296335da4262858b9d4239cebd7576",
    "zh:a1c7c65ab3565e0a88a62b56427476a7866222f9f8b68f9cd616919bc36ddf9c",
    "zh:ad685238fd65423a30ccee23f105fda2fc02d5dd262662fad635b2d50266455d",
    "zh:bad3e49f50bbd5c6fb07248fdf26e05924e8f4be6c039d6a7e0fe9d5a2e9822b",
    "zh:d4652bbbfdf95bfb42549232031b3576dcab21ffe19e42486a1d62b7821298ae",
    "zh:e2a083af570fb740032f510f297d68ddbe71c3ea26733356061f5956eedc5b63",
    "zh:ea29e8ad1826394e0546a85f59f495094ac2e864248d49e980161f87efd0d008",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:0ULxM8/DscMzfiDWg1yclBf/39U44wQmlx745BfYZ80=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}
