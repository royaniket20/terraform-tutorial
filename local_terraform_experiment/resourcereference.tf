resource "random_pet" "mypet_ref" {
  prefix    = "Mrs"
  separator = "*"
  length    = 3
}

resource "local_file" "pets_ref" {
  filename        = "RandomPet.txt"
  content         = "We love Aniket Roy and My Pet is - ${random_pet.mypet_ref.id}" //Implict Dependency
  file_permission = "0700"
  depends_on = [
    random_pet.mypet_ref
  ]
  //Explicit Dependency
}

output "random-per-name" {
  value       = random_pet.mypet_ref.id
  description = "Generated Random Pet Name"
}
